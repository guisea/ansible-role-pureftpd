Ansible Role - Pure-FTPD
=========

A role to provision a Pure-FTPD based FTP service.

Requirements
------------

Ansible >= 2.6

Role Variables
--------------

A description of the settable variables for this role should go here, including
any variables that are in defaults/main.yml, vars/main.yml, and any variables
that can/should be set via parameters to the role. Any variables that are read
from other roles and/or the global scope (ie. hostvars, group vars, etc.) should
be mentioned here as well.

```yaml
# defaults file for ansible-role-pureftpd
pureftpd_chroot_everyone: 'yes'
# Yes by default - Disallow anonymous connections
pureftpd_no_anonymous: 'yes'
# Maximum Clinets Per IP Address
pureftpd_max_clients_per_ip: 8
# Do you want TLS (FTPS)? [true|false]
pureftpd_enable_tls: false
pureftpd_cert_from_variable: true
## ** Recommended ** Populates the certificate file "/etc/ssl/private/pure-ftpd.pem" from content in a variable suggest
## using Ansible Vault. See https://docs.ansible.com/ansible/latest/user_guide/vault.html
# pureftpd_cert: "{{ }}"
## Copies cert file from files directory of playbook
# pureftpd_certfile:
pureftpd_tls_cert_file: /etc/ssl/private/pure-ftpd.pem
# If TLS Enabled populate the below
## Default refuse connections that don't use SSL/TLS security mechanisms
pureftpd_tls_mode: 2
purefptd_tls_cipher_suite: HIGH
    
# Passive Port Range
pureftpd_passive_range: 30000 50000
```

    

Dependencies
------------

Nil

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables
passed in as parameters) is always nice for users too:

    - hosts: ftp
      roles:
         - role: ansible-role-pureftpd
           pureftpd_enable_tls: true 

License
-------

BSD

Author Information
------------------

Aaron Guise 
An optional section for the role authors to include contact information, or a
website (HTML is not allowed).
