import os

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_hosts_file(host):
    f = host.file('/etc/hosts')

    assert f.exists
    assert f.user == 'root'
    assert f.group == 'root'


def test_pureftpd_installed(host):
    pureftpd = host.package("pure-ftpd")
    assert pureftpd.is_installed


def test_pureftpd_running_and_enabled(host):
    pureftpd = host.service("pure-ftpd")
    assert pureftpd.is_running
    assert pureftpd.is_enabled
